from django.shortcuts import render, get_object_or_404
from django.http import JsonResponse
from similar_text import similar_text
from .models import *

def index(request):
    gejala = Gejala.objects.all()
    response = {'lists': gejala}
    return render(request, "./cari_data.html", response)

def polls_list(request):
    MAX_OBJECTS = 20
    polls = Gejala.objects.all()[:MAX_OBJECTS]
    data = {"results": list(polls.values("question", "created_by__username", "pub_date"))}
    return JsonResponse(data)

def gejala(request):
    MAX_OBJECTS =30
    gejala = Gejala.objects.all()[:MAX_OBJECTS]
    data = {
        "status_code": 200,
        "message": "Success",
        "result": list(gejala.values("id", "nama_gejala"))
    }
    return JsonResponse(data)

def penyakit(request):
    MAX_OBJECTS =30
    penyakit = Penyakit.objects.all()[:MAX_OBJECTS]
    data = {
        "status_code": 200,
        "message": "Success",
        "result": list(penyakit.values("id", "nama_penyakit"))
    }
    return JsonResponse(data)

def pola(request):
    MAX_OBJECTS =30
    pola = Pola.objects.all()[:MAX_OBJECTS]
    data = {
        "status_code": 200,
        "message": "Success",
        "result": list(pola.values("id", "pola", "id_penyakit"))
    }
    return JsonResponse(data)

def kesimpulan(request):
    MAX_OBJECTS =30
    pola_id = request.GET.get('pola')
    pola = Pola.objects.get(pola=str(pola_id))[:MAX_OBJECTS]
    data = {
        "status_code": 200,
        "message": "Success",
        "result": list(pola.values("id", "pola", "id_penyakit"))
    }
    return JsonResponse(data)


def search(request):
    # get_data = request.POST.get("cari")
    # print(get_data)
    if request.POST:
        ab=""
        get_data1 = request.POST.getlist("cari")
        for a in get_data1:
            ab += a + ","
        get_data = ab[:-1]
        pola_sql = Pola.objects.filter(pola=get_data)
        print("count sql: " + str(len(pola_sql)))
        if len(pola_sql) > 0:
            pola1 = pola_sql.first()
            nama_penyakit = pola1.id_penyakit
            persentase = 100
            print("masuk if : "+ str(nama_penyakit))
        else :
            pola_sql2 = Pola.objects.all()
            nilai_kesamaan_awal = 0
            nama_penyakit1 = ""
            for polas in pola_sql2 :
                nilai_kesamaan = similar_text(str(polas.pola), str(get_data))
                if nilai_kesamaan > nilai_kesamaan_awal :
                    nilai_kesamaan_awal = nilai_kesamaan
                    nama_penyakit1 = polas.id_penyakit
                else :
                    nilai_kesamaan_awal = nilai_kesamaan_awal
                print("awal : "+str(nilai_kesamaan)+" & terakhir : "+str(nilai_kesamaan_awal) + " & penyakit : "+str(nama_penyakit1))
            nama_penyakit = nama_penyakit1
            persentase = nilai_kesamaan_awal
        print("persentase kesamaan penyakit : " + str(persentase) + "% & nama penyakit : " + str(nama_penyakit) )
        if persentase < 70:
            nama_penyakit = "Persentase kemungkinan < 70% / tidak ada didatabase"
            persentase = 0
        data = {
            "status_code": 200,
            "message": "Success",
            "result": [
                {
                    "nama_penyakit" : str(nama_penyakit),
                    "persentase" : str(persentase)+"%"
                }
            ] 
        }
        return JsonResponse(data)
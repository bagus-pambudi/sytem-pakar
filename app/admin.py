from django.contrib import admin
from import_export.admin import ImportExportModelAdmin
from .models import *

# Register your models here.
class GejalaAdmin(ImportExportModelAdmin):
    list_display = ('id', 'nama_gejala')
    search_fields = ['nama_gejala']
    pass
admin.site.register(Gejala, GejalaAdmin)

class PenyakitAdmin(ImportExportModelAdmin):
    list_display = ('id', 'nama_penyakit')
    search_fields = ['nama_penyakit']
    pass
admin.site.register(Penyakit, PenyakitAdmin)

class PolaAdmin(ImportExportModelAdmin):
    list_display = ('id', 'pola', 'id_penyakit')
    search_fields = ['pola']
    list_filter = ['id_penyakit']
    pass
admin.site.register(Pola, PolaAdmin)
from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('api/v1/gejala/', views.gejala, name='gejala'),
    path('api/v1/penyakit/', views.penyakit, name='penyakit'),
    path('api/v1/pola/', views.pola, name='pola'),
    path('api/v1/cari_data/', views.search, name='search'),
    path('api/v1/cari/<pola_id>/', views.kesimpulan, name='kesimpulan')

]
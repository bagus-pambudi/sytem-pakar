from django.db import models

# Create your models here.
class Gejala(models.Model):
    nama_gejala = models.CharField(max_length=255)
    class Meta:
       ordering = ["id"]

    def __str__(self):
        return self.nama_gejala
    
class Penyakit(models.Model):
    nama_penyakit = models.CharField(max_length=255)
    class Meta:
       ordering = ["id"]

    def __str__(self):
        return self.nama_penyakit

class Pola(models.Model):
    pola = models.CharField(max_length=255,  help_text="pola dengan id gejala example : 1,2,3")
    id_penyakit = models.ForeignKey(Penyakit, on_delete=models.CASCADE)
    class Meta:
       ordering = ["id"]
    def __str__(self):
        return self.pola
    